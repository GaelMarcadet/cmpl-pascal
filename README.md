# Compilateur Cascal vers P-Code

Ce projet consiste en la création d'un compilateur permettant de compiler
un code pascal en P-Code.

J'attire toutefois l'attention du lecteur sur le fait que le pascal compilé ici 
diffère dans sa synthaxe, de celle communément admise.
On parlera alors de Pascal pour désigner Cascal (ou alors C-Pascal) qui est la version explicité dans ce README.

## Fonctionnalités

Le compilateur offre des fonctionnalités semblables à celui d'un compilateur pascal.
- Expression : `a := 1 + 2 * 3 + ( 4  * 23 / 2 )`
    - On retrouve les opérateurs booléeans: TRUE, FALSE, &&, ||
    - On retrouve les opérateurs de comparaison: `==`, `!=`, `<`, `>`, `<=`, `>=`
    - Les opérateurs arythmétiques: `+`, `-`, `*`, `/`, `%` avec la gestion des parenthèses.
    - Les booléens: `TRUE`et `FALSE`
    - L'opérateur ternaire: `<expr> ? <expr> : <expr>`
    - Le tableau: `[ 1, 2, 3 ]`
- Structures de données :
    - Les records
    - Les types
    - Les tableaux
- Instructions:
    - Affectation : `a := 1`
    - Structure conditionnelle: `if .. then ... ( else ... )`
    - Structure de boucle:
        - Boucle `while`
        - Boucle `do until`
        - Boucle `for`
    - Structure `switch` 
- Procédures :
    - Sans arguments
    - Avec arguments :
        - Passage par valeur
        - Passage par adresse
    - Avec type de retour, défini par le mot clé `return <expr>`
    - Les procédures imbriquées.
    
- Divers :
    - Les commentaires
    - Lire / Ecrire depuis l'entrée standard

Toutes les fonctionnalités présentées sont exposées
dans des programmes de démonstration disponible dans le dossier *res/samples*.

Le compilateur assure également quelques gestions des erreurs, notamment:
- Accès  à un symbol non défini
- Accès à une fonction non définie
- Vérification du nombre de paramètres
- Vérification du type de retour par rapport à celui déclaré 
- Vérification du type lors d'une affectation
- Vérification du type de paramètres

Un programme erroné contenant bon nombre d'erreurs est disponible dans le fichier *res/samples/errors.src*.