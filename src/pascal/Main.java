package pascal;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import pascal.grammar.PascalLexer;
import pascal.grammar.PascalParser;

public class Main {

    public static void main(String[] args) throws Exception {
        String file = args.length==0 ? "res/samples/functions.src" : args[0];

        CharStream input = CharStreams.fromFileName(file);
        PascalLexer lexer = new PascalLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        PascalParser parser = new PascalParser(tokens);
        Environement env = parser.parse();
        if ( !env.hasError() ) {
            PMachine machine = new PMachine( env, env.getInstructions().toArray( new Instruction[]{} ) );
            System.out.println(machine);
            machine.exec();

            System.out.println(" =================== OUTPUT ================== ");
            System.out.println( machine.getOutput() );
        } else {
            ParserErrorManager errorManager = env.getErrorsManager();
            for ( String error : errorManager.getErrors() ) {
                System.err.println( "Error: " + error );
            }
        }
    }
}
