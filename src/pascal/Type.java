package pascal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public interface Type {
    int getTaille();
    boolean callable();
    boolean indexable();

    static class PrimitiveType implements Type {
        public int getTaille() {
            return 1;
        }

        @Override
        public boolean callable() {
            return false;
        }

        @Override
        public boolean indexable() {
            return false;
        }
    }

    public static class Entier extends PrimitiveType {
    }

    public static class Bool extends PrimitiveType {
    }



    public static class Tableau implements Type {
        public int taille;
        public Type type;

        public Tableau(int taille, Type type) {
            this.taille = taille;
            this.type = type;
        }

        public int getTaille() {
            return taille*type.getTaille();
        }

        @Override
        public boolean callable() {
            return false;
        }

        @Override
        public boolean indexable() {
            return true;
        }
    }

    public static class Enregistrement implements Type {
        HashMap< String, Type > fields;

        public Enregistrement() {
            this( new HashMap<>() );
        }

        public Enregistrement( HashMap< String, Type > fields ) {
            this.fields = fields;
        }

        public void addField( String fieldName, Type fieldType ) {
            this.fields.put( fieldName, fieldType );
        }

        public boolean fieldExists( String field ) {
            return this.fields.containsKey( field );
        }

        public int getTaille() {
            int res = 0;
            for( Type t : fields.values() ) {
                res += t.getTaille();
            }
            return res;
        }


        public int getAdresse(String nom) {
            int res = 0;
            for ( Map.Entry< String, Type > field : fields.entrySet() ) {
                if ( field.getKey().equals( nom ) ) {
                    return res;
                }
                res += field.getValue().getTaille();
            }
            return -1;
        }

        public Type getType(String nom) {
            return this.fields.get( nom );
        }

        @Override
        public boolean callable() {
            return false;
        }

        @Override
        public boolean indexable() {
            return false;
        }
    }

    public static class Proc implements Type {
        public ArrayList<Parametre> param = new ArrayList<Parametre>();
        public Type returnedType;
        public int taille=0;

        public void setReturnedType( Type type ) {
            this.returnedType = type;
        }

        public Type getReturnedType() {
            return this.returnedType;
        }

        @Override
        public int getTaille() {
            return taille;
        }
        @Override
        public boolean callable() {
            return true;
        }

        @Override
        public boolean indexable() {
            return false;
        }

        public Type getParamType( int paramIndex ) {
            return param.get( paramIndex ).type;
        }

        public int countParameters() {
            return this.param.size();
        }

        public int parametersSize() {
            int size = 0;
            for ( Parametre parametre : param ) {
                size += parametre.getTaille();
            }
            return size;
        }

        public boolean paramHasValue( int paramIndex ) {
            if ( paramIndex < this.countParameters() ) {
                return this.param.get( paramIndex ).parValeur;
            }
            return true;
        }
    }

    public static class Parametre implements Type {
        public Type type;
        public boolean parValeur;

        public Parametre(Type type, boolean parValeur) {
            this.type = type;
            this.parValeur = parValeur;
        }

        @Override
        public int getTaille() {
            return parValeur?type.getTaille():1;
        }

        @Override
        public boolean callable() {
            return type.callable();
        }

        @Override
        public boolean indexable() {
            return type.indexable();
        }
    }

    public class Void implements Type {
        @Override
        public int getTaille() {
            return 0;
        }

        @Override
        public boolean callable() {
            return false;
        }

        @Override
        public boolean indexable() {
            return false;
        }
    }

    public class Unkown implements Type {
        public Unkown() {
        }

        @Override
        public int getTaille() {
            return 0;
        }

        @Override
        public boolean callable() {
            return false;
        }

        @Override
        public boolean indexable() {
            return false;
        }
    }
}
