package pascal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SymbolsTable {

    private final Environement env;
    //private HashMap<String,Variable> table = new HashMap<String, Variable>();
    private final List< Level > table = new ArrayList<>();
    private Level currentLevel;


    public SymbolsTable( Environement env ) {
        this.env = env;
        // initialize symbols t
        this.currentLevel = new Level();
        this.table.add( 0, currentLevel );
    }

    public boolean isGlobal() {
        return this.table.size() == 1;
    }


    public void upLevel(){
        if ( !this.isGlobal() ) {
            // pop out the last level and put the new top item at current level
            this.table.remove( 0 );
            this.currentLevel = this.table.get( 0 );
        } else {
            throw new RuntimeException( "Cannot level up at main level !" );
        }
    }

    public void downLevel(){
        this.currentLevel = new Level();
        this.table.add( 0, currentLevel );
    }

    public void requireReturnInstructionIsInProc() {
        if ( this.isGlobal() ) {
            this.env.getErrorsManager().addError( "Cannot return at main level" );
        }
    }



    public Variable get(String nom) {
        Variable res = null;
        for ( Level level : this.table ) {
            if ( level.isDefined( nom ) ) {
                return level.getVariable( nom );
            }
        }

        return null;
    }

    public Map< String, Variable > getSymbols() {
        return this.table.get( this.table.size() - 1 ).table;
    }



    public boolean symbolExists( String symbol ) {
        for ( Level level : this.table ) {
            if ( level.isDefined( symbol ) ) {
                return true;
            }
        }
        return false;
    }

    public void put(String nom, Type type) {
        this.currentLevel.put( nom, type );
    }

    public Type getType( String symbol ) {
        for ( Level level : this.table ) {
            if ( level.isDefined( symbol ) ) {
                return level.getVariable( symbol ).type;
            }
        }
        throw new RuntimeException( "Trying to access at symbol which is not defined: " + symbol );
    }

    public void putParametre(String nom, Type.Parametre type,int adresse) {
        this.currentLevel.putParameter( nom, type, adresse );
    }

    public void putProc(String nom, Type.Proc type,int adresse) {
        this.currentLevel.putProc( nom, type, adresse );
    }

    public int getTaille() {
        return this.currentLevel.getSize();
    }





    
    public void requireSymbolExists( String symbol ) {
        if ( !this.symbolExists( symbol ) ) {
            this.env.getErrorsManager().addError( "Undefined symbol: " + symbol );
        }
    }

    public void requireSymbolAsArray( String symbol ) {
        this.requireSymbolExists( symbol );
        if ( this.symbolExists( symbol ) ) {
            Type type = getType( symbol );
            if ( !type.indexable() ) {
                this.env.getErrorsManager().addError( "Cannot cast symbol " + symbol + " as array" );
            }
        }
    }

    public void requireSymbolAsRecord( String symbol ) {
        this.requireSymbolExists( symbol );
        if ( this.symbolExists( symbol ) ) {
            Type type = getType( symbol );
            if ( !( type instanceof Type.Enregistrement ) ) {
                this.env.getErrorsManager().addError( "Cannot cast symbol " + symbol + " as record" );
            }
        }
    }

    public void requireRecordFieldExists( String symbol, Type.Enregistrement enregistrement, String field ) {
        if ( !enregistrement.fieldExists( field ) ) {
            this.env.getErrorsManager().addError( "Access error: Cannot accessed to field " + field + " in record for symbol " + symbol );
        }
    }

    public void requireSymbolAsProcedure( String symbol ) {
        if ( this.symbolExists( symbol ) && !getType( symbol ).callable() ) {
            this.env.getErrorsManager().addError( "Cannot call symbol " + symbol + " as procedure" );
        }
    }

    public void requireParamHasType( Type.Proc proc, int paramIndex, Type givenParamType ) {
        if ( proc != null ) {
            if ( !givenParamType.getClass().equals( proc.getParamType( paramIndex ).getClass() )  ) {
                this.env.getErrorsManager().addError( "Illegal procedure calling" );
            }
        }

    }

    public void requireValidNumberOfParameters( String functionName, Type.Proc proc, int givenParametersNumber ) {
        // if proc is undefined but registred as function, try to retreive it
        if ( proc == null && symbolExists( functionName ) ) {
            Type foundType = this.getType( functionName );
            if ( foundType.callable() ) {
                proc = ( Type.Proc ) foundType;
            }
        }

        // checks number of param
        if ( proc != null ) {
            if ( proc.countParameters() != givenParametersNumber ) {
                this.env.getErrorsManager().addError( "Illegal number of parameters given while calling " + functionName + ": " + proc.countParameters() +" required but " + givenParametersNumber + " given" );
            }
        }
    }



    private class Level {
        private final HashMap< String, Variable > table;
        private int size;

        public Level() {
            this.table = new HashMap<>();
            this.size = 0;
        }

        public void put( String symbol, Type type ) {
            table.put( symbol, new Variable( size, type, isGlobal() ) );
            size += type.getTaille();
        }

        public void putParameter( String symbol, Type.Parametre parameterType, int address ) {
            this.table.put( symbol, new Variable( address, parameterType,false) );
        }

        public void putProc( String procName, Type.Proc type, int address ) {
            // table.put(nom,new Variable(adresse,type,true));
            this.table.put( procName,  new Variable( address, type, isGlobal() ) );
        }



        public boolean isDefined( String symbol ) {
            return this.table.containsKey( symbol );
        }

        public Variable getVariable( String symbol ) {
            return this.table.get( symbol );
        }

        public int getSize() {
            return this.size;
        }
    }



}
