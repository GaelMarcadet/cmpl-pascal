package pascal;

public class Variable {
    public int adresse;
    public Type type;
    public boolean isGlobal;

    public Variable(int adresse, Type type,boolean isGlobal) {
        this.adresse = adresse;
        this.type = type;
        this.isGlobal=isGlobal;
    }

    public int getAdresse() {
        return adresse;
    }

    public void setAdresse( int adresse ) {
        this.adresse = adresse;
    }

    public Type getType() {
        return type;
    }

    public void setType( Type type ) {
        this.type = type;
    }

    public boolean isGlobal() {
        return isGlobal;
    }

    public void setGlobal( boolean global ) {
        isGlobal = global;
    }
}
