package pascal;

import java.util.ArrayList;
import java.util.Collection;

public class ParserErrorManager {
    private Collection< String > errors = new ArrayList<>();

    public Collection< String > getErrors() {
        return this.errors;
    }

    public boolean hasErrors() {
        return !this.errors.isEmpty();
    }

    public void notifyDeclarationMissing( String symbol ) {
        this.errors.add( "Symbol declaration missing: " + symbol );
    }

    public void addError( String error ) {
        this.errors.add( error );
    }
}
