package pascal;

import java.util.HashMap;

public class TypesTable {
    private HashMap< String, Type > typesTable = new HashMap<>();

    public boolean typeExists( String type ) {
        return this.typesTable.containsKey( type );
    }

    public Type getType( String type ) {
        return this.typesTable.get( type );
    }

    public void registerType( String name, Type type ) {
        this.typesTable.put( name, type );
    }
}
